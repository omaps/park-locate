package googlemaps.devstream.com.mapsgoogle;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import org.w3c.dom.Document;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import db.DatabaseOperations;
import utils.AddressFromLatLng;
import utils.DataHolder;
import utils.Distance;
import utils.GoogleMapRoute;
import utils.MarkerDataSource;
import utils.MyMarkerObject;

/**
 * Created by David Gormally on 11/06/15.
 */

public class MapsActivity extends ActionBarActivity implements LocationListener, SensorEventListener {

    private GoogleMap googleMap;
    LatLng myPosition;
    LatLng latLng;
    private LocationManager locationManager;
    private String provider;
    MarkerOptions marker;
    Marker secondMarker;
    private ImageButton parkCar, cancel, directions, rate;
    double latitude;
    double longitude;
    LatLng fromPosition, toPosition;
    Context ctx = this;
    DatabaseOperations dbOpaerations = new DatabaseOperations(ctx);
    public AddressFromLatLng ad;
    public int lineColor = 0xFF00B3FD;
    private ImageView compass;
    private float currentDegree = 0f;
    private SensorManager mSensorManager;
    private CountDownTimer timer;
    MarkerDataSource markerDataSource;
    private PolylineOptions rectLine;
    private ArrayList<LatLng> directionPoint;
    private Typeface myTypeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        compass = (ImageView) findViewById(R.id.compass);
        myTypeface = Typeface.createFromAsset(getAssets(), "fonts/bebasneue.otf");

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);

        Display display = wm.getDefaultDisplay();
        Point screen = new Point();
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        } else {
            screen.x = display.getWidth();
            screen.y = display.getHeight();
        }
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setLogo(R.drawable.bluelogooo);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setTitle(Html.fromHtml("<medium><font color='#3598db'>&nbsp; Park And Locate </font></medium>"));
        ad = new AddressFromLatLng();
        parkCar = (ImageButton) findViewById(R.id.park_car);
        parkCar.setBackgroundResource(R.drawable.parkhere);
        cancel = (ImageButton) findViewById(R.id.cancel);
        cancel.setBackgroundResource(R.drawable.cancelblack);
        directions = (ImageButton) findViewById(R.id.directions);
        directions.setBackgroundResource(R.drawable.getdirection);
        rate = (ImageButton) findViewById(R.id.rate);
        rate.setBackgroundResource(R.drawable.ratemeblack);
        parkCar.setOnClickListener(new ButtonClick());
        cancel.setOnClickListener(new ButtonClick());
        directions.setOnClickListener(new ButtonClick());
        rate.setOnClickListener(new ButtonClick());
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        googleMap = fm.getMap();
        markerDataSource = new MarkerDataSource(ctx);
        try {
            markerDataSource.open();
            //dbOpaerations.deleteAll();
            //53.726408,-6.340239
            //dbOpaerations.testInsert(dbOpaerations, 53.726408, -6.340239);
            //dbOpaerations.close();
        } catch (Exception e) {

        }
        if (dbOpaerations.isParked()) {
            parkCar = (ImageButton) findViewById(R.id.park_car);
            parkCar.setBackgroundResource(R.drawable.parkblue);
            parkCar.setEnabled(false);
            List<MyMarkerObject> m = markerDataSource.getMarkers();
            for (int i = 0; i < m.size(); i++) {
                String lasd = m.get(i).getLongit();
                String lit = m.get(i).getLatitude();
                LatLng lat = new LatLng((Double.valueOf(lit)), Double.valueOf(lasd));
                marker = new MarkerOptions().position(lat);
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.bluecar2n));
                marker.anchor(0.5f, 0.62f);
                googleMap.addMarker(marker);
                cancel.setEnabled(true);
            }
        }
        currentLocationFinder();
    }


    public void currentLocationFinder() {
        Criteria criteria = new Criteria();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            //53.3409844,-6.2583369,
            latLng = new LatLng(latitude, longitude);
            DataHolder.getDataHolder().setParkingCoords(latLng);
            myPosition = new LatLng(latitude, longitude);
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(10));
            googleMap.setMyLocationEnabled(true);
            googleMap.setIndoorEnabled(true);
            googleMap.getUiSettings().setCompassEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            googleMap.setBuildingsEnabled(true);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f));
            AddressFromLatLng.getFullAddress(MapsActivity.this, latLng);
        }
    }


    private void addParkedMarker() {
        googleMap.clear();
        marker = new MarkerOptions().position(new LatLng(latitude, longitude));
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.bluecar2n));
        marker.anchor(0.5f, 0.62f);
        googleMap.addMarker(marker);
    }

    public void onBackPressed() {
        moveTaskToBack(true);
    }

    private void setTheRoute(LatLng latLng) {
        DatabaseOperations dop = new DatabaseOperations(ctx);
        Cursor cr = dop.getSavedLocation(dop);
        cr.moveToFirst();
        double a = cr.getDouble(0);
        double b = cr.getDouble(1);
        //53.726408,-6.340239
        //fromPosition = new LatLng(latitude, longitude);
        fromPosition = new LatLng(latitude, longitude);
        toPosition = new LatLng(a, b);
        if ((latitude != 0 && longitude != 0)) {
            googleMap = ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map)).getMap();
            GoogleMapRoute md = new GoogleMapRoute();
            googleMap.addMarker(new MarkerOptions().position(fromPosition).title("Start"));
            secondMarker = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude))
                    .title("Start"));
            Document doc = md.getDocument(fromPosition, toPosition, GoogleMapRoute.MODE_WALKING);
            directionPoint = md.getDirection(doc);
            rectLine = new PolylineOptions().width(13f).color(lineColor);
            for (int i = 0; i < directionPoint.size(); i++) {
                rectLine.add(directionPoint.get(i));
            }
            googleMap.addPolyline(rectLine);
        } else {
            directions = (ImageButton) findViewById(R.id.directions);
            directions.setBackgroundResource(R.drawable.getdirection);
            Toast.makeText(MapsActivity.this, "Directions not found. Please check your location settings",
                    Toast.LENGTH_LONG).show();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }


    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME);
    }


    protected boolean isRouteDisplayed() {
        // TODO Auto-generated method stub
        return false;
    }


    @Override
    public void onLocationChanged(Location location) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location
                .getLatitude(), location.getLongitude())));
        currentLocationFinder();
    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // get the angle around the z-axis rotated 
        float degree = Math.round(event.values[0]);
        //create a rotation animation (reverse turn degree degrees) 
        RotateAnimation ra = new RotateAnimation(currentDegree,
                -degree, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f);
        ra.setDuration(210);
        ra.setFillAfter(true);
        compass.startAnimation(ra);
        currentDegree = -degree;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }


    private class ButtonClick implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.park_car:
                    parkCar = (ImageButton) findViewById(R.id.park_car);
                    parkCar.setBackgroundResource(R.drawable.parkblue);
                    cancel = (ImageButton) findViewById(R.id.cancel);
                    cancel.setBackgroundResource(R.drawable.cancelblack);
                    dbOpaerations.deleteAll();
                    showDialogToPark();
                    break;
                case R.id.cancel:
                    if(dbOpaerations.isParked()) {
                        showDialogToCancel();
                    }
                    break;
                case R.id.directions:
                    if (dbOpaerations.isParked()) {
                        directions = (ImageButton) findViewById(R.id.directions);
                        directions.setBackgroundResource(R.drawable.blue2);
                        cancel = (ImageButton) findViewById(R.id.cancel);
                        cancel.setBackgroundResource(R.drawable.cancelblack);
                        setTheRoute(latLng);
                        CustomToast();
                    } else {
                        Toast.makeText(MapsActivity.this, "You are not parked anywhere", Toast.LENGTH_LONG).show();
                    }
                    break;
                case R.id.rate:
                    break;

            }
        }
    }



    private void showDialogToPark() {
        if (latitude != 0 && longitude != 0) {
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.activity_dialog);
            TextView address = (TextView) dialog.findViewById(R.id.address11);
            TextView address2 = (TextView) dialog.findViewById(R.id.city);

            address.setText(AddressFromLatLng.getAddressLineOne(MapsActivity.this,
                    DataHolder.getDataHolder().getParkingCoords()));

            address2.setText(AddressFromLatLng.getAddressLineTwo(MapsActivity.this,
                    DataHolder.getDataHolder().getParkingCoords()));

            Button cancel = (Button) dialog.findViewById(R.id.button_close);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    parkCar = (ImageButton) findViewById(R.id.park_car);
                    parkCar.setBackgroundResource(R.drawable.parkhere);
                    parkCar.setEnabled(true);
                    dialog.dismiss();
                }
            });
            Button park = (Button) dialog.findViewById(R.id.button_park);
            park.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dbOpaerations = new DatabaseOperations(ctx);
                    dbOpaerations.saveLocation(dbOpaerations, latitude, longitude);
                    addParkedMarker();
                    dialog.dismiss();

                    Toast.makeText(MapsActivity.this, "Parking location saved", Toast.LENGTH_LONG).show();


                }
            });
            dialog.show();
        } else {
            parkCar = (ImageButton) findViewById(R.id.park_car);
            parkCar.setBackgroundResource(R.drawable.parkhere);
            parkCar.setEnabled(true);
            Toast.makeText(MapsActivity.this, "Bad Signal, Please Try Again", Toast.LENGTH_LONG).show();
        }
    }

    public void showDialogToCancel(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.cancel_dialog);
        Button unpark = (Button) dialog.findViewById(R.id.button_cancel);
        unpark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbOpaerations.deleteAll();
                cancel = (ImageButton) findViewById(R.id.cancel);
                cancel.setBackgroundResource(R.drawable.cancelblue);
                parkCar = (ImageButton) findViewById(R.id.park_car);
                parkCar.setBackgroundResource(R.drawable.parkhere);
                directions = (ImageButton) findViewById(R.id.directions);
                directions.setBackgroundResource(R.drawable.getdirection);
                parkCar.setEnabled(true);
                googleMap.clear();
                dialog.dismiss();
                Toast.makeText(MapsActivity.this, "Parking location deleted", Toast.LENGTH_LONG).show();

            }
        });

        Button dismissBtn = (Button) dialog.findViewById(R.id.button_close_cancel);

        dismissBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                cancel = (ImageButton) findViewById(R.id.cancel);
                cancel.setBackgroundResource(R.drawable.cancelblack);
                cancel.setEnabled(true);
            }
        });

        dialog.show();

    }



    //@TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void CustomToast() {
        Distance ds = new Distance();
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.activity_toast,
                (ViewGroup) findViewById(R.id.toast_layout_root));
        TextView distance = (TextView) layout.findViewById(R.id.distance);
        //layout.setBackgroundColor(getResources().getColor(R.color.toast_one));
        //layout.setAlpha(0.4f);
        TextView address = (TextView) layout.findViewById(R.id.address);
        TextView city = (TextView) layout.findViewById(R.id.city);
        DecimalFormat oneDigit = new DecimalFormat("#,##0.0");
        double dis = ds.CalculationByDistance(fromPosition, toPosition);
        String a = oneDigit.format(dis);
        String k = " KM";
        distance.setText(a + k);
        distance.setTypeface(myTypeface);

        address.setText(AddressFromLatLng.getAddressLineOne(MapsActivity.this,
               toPosition));

        city.setText(AddressFromLatLng.getAddressLineTwo(MapsActivity.this,
                toPosition));

        final Toast toast = new Toast(getApplicationContext());
        if (latitude == 0 && longitude == 0) {
        } else {
            toast.setView(layout);
            //toast.setGravity(Gravity.CENTER, 0, 570);
            toast.setGravity(Gravity.CENTER | Gravity.BOTTOM, 0, 270);

            timer = new CountDownTimer(5000, 1000) {
                public void onTick(long millisUntilFinished) {
                    toast.show();
                }

                public void onFinish() {
                    toast.cancel();
                }
            }.start();
        }
    }
}
