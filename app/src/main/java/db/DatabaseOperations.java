package db;

/**
 * Created by David Gormally on 14/06/15.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DatabaseOperations extends SQLiteOpenHelper {

    /**
     * Database name
     */
    private static String DBNAME = "locationmarkersqlite";

    /**
     * Version number of the database
     */
    private static int VERSION = 2;

    /**
     * Field 1 of the table locations, which is the primary key
     */
    public static final String FIELD_ROW_ID = "_id";

    /**
     * Field 2 of the table locations, stores the latitude
     */
    public static final String FIELD_LAT = "lat";

    /**
     * Field 3 of the table locations, stores the longitude
     */
    public static final String FIELD_LNG = "lng";

    /**
     * A constant, stores the the table name
     */
    //private static final String DATABASE_TABLE = "locations";

    /*String sql = "create table " +DATABASE_TABLE+ " ( " +
            FIELD_ROW_ID+ " integer primary key autoincrement , " +
            FIELD_LAT+ " TEXT, " +
            FIELD_LNG+ " TEXT)";*/
    Cursor cr;

    public String CREATE_QUERY = "CREATE TABLE IF NOT EXISTS " +
            "" + TableData.TableInfo.TABLE_NAME + "(" + TableData.TableInfo.LATITUDE + " TEXT," + TableData.TableInfo.LONGITUDE + " TEXT);";

    public DatabaseOperations(Context context) {
        super(context, TableData.TableInfo.DATABASE_NAME, null, VERSION);
        // TODO Auto-generated constructor stub
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        try {
            db.execSQL(CREATE_QUERY);
            System.out.println(CREATE_QUERY);
        } catch (Exception e) {
            System.out.println("Unable to create database");
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        String dropTable = "DROP TABLE IF EXISTS" + TableData.TableInfo.TABLE_NAME;
        db.execSQL(dropTable);
        //onCreate(db);
    }

    public void testInsert(DatabaseOperations dop, double lat, double lng){
        SQLiteDatabase SQ = dop.getReadableDatabase();
        ContentValues cv = new ContentValues();
        //53.701858,-6.310487
        try {
            cv.put(TableData.TableInfo.LATITUDE, lat);
            cv.put(TableData.TableInfo.LONGITUDE, lng);
            SQ.insert(TableData.TableInfo.TABLE_NAME, null, cv);
            Log.d("Database operations", "1 row inserted");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void saveLocation(DatabaseOperations dop, double latitute, double longit) {
        SQLiteDatabase SQ = dop.getReadableDatabase();
        ContentValues cv = new ContentValues();
        try {
            cv.put(TableData.TableInfo.LATITUDE, latitute);
            cv.put(TableData.TableInfo.LONGITUDE, longit);
            SQ.insert(TableData.TableInfo.TABLE_NAME, null, cv);
            Log.d("Database operations", "1 row inserted");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Cursor getSavedLocation(DatabaseOperations dop) {
        SQLiteDatabase SQ = dop.getReadableDatabase();
        String[] columns = {TableData.TableInfo.LATITUDE, TableData.TableInfo.LONGITUDE};
        cr = SQ.query(TableData.TableInfo.TABLE_NAME, columns, null, null, null, null, null);
        System.out.println(cr);
        return cr;
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TableData.TableInfo.TABLE_NAME, null, null);
        Log.d("Database operations", "Table deleted");
        db.close();
    }

    public Boolean isParked() {
        SQLiteDatabase SQ = this.getReadableDatabase();
        String[] columns = {TableData.TableInfo.LATITUDE, TableData.TableInfo.LONGITUDE};
        cr = SQ.query(TableData.TableInfo.TABLE_NAME, columns, null, null, null, null, null);
        if (cr.getCount() > 0) {
            return true;
        } else
            return false;
    }

    public void dbDelete(){
        SQLiteDatabase db = this.getWritableDatabase();

    }
}




