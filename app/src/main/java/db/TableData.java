package db;

import android.provider.BaseColumns;

/**
 * Created by David Gormally on 14/06/15.
 */
public class TableData {

    public TableData(){
    }

    public static abstract class TableInfo implements BaseColumns
    {

        public static final String LATITUDE = "latitude" ;
        public static final String LONGITUDE = "longitude" ;
        public static final String DATABASE_NAME = "userdb97";
        public static final String TABLE_NAME = "regtb97";
    }
}
