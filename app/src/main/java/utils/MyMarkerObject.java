package utils;

import java.util.List;

/**
 * Created by David Gormally on 23/06/15.
 */
public class MyMarkerObject {

    private String longit;
    private String lat;

    public String getLongit(){
        return longit;
    }

    public void setLongit(String longit){
        this.longit=longit;
    }


    public String getLatitude(){
        return lat;
    }

    public void setLatitude(String lat){
        this.lat=lat;
    }
}
