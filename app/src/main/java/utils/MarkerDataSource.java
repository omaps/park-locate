package utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseOperations;
import db.TableData;

/**
 * Created by David Gormally on 23/06/15.
 */
public class MarkerDataSource {

    DatabaseOperations db;
    SQLiteDatabase sqLiteDatabase;
    String[] cols = {TableData.TableInfo.LATITUDE, TableData.TableInfo.LONGITUDE};

    public MarkerDataSource(Context context){
        db = new DatabaseOperations(context);
    }

    public void open() throws SQLException{
        sqLiteDatabase = db.getReadableDatabase();
    }

    public void close(){
        sqLiteDatabase.close();
    }

    public void addMarker(MyMarkerObject obj){
        ContentValues v = new ContentValues();
        v.put(DatabaseOperations.FIELD_LAT, obj.getLatitude());
        v.put(DatabaseOperations.FIELD_LAT, obj.getLongit());
        sqLiteDatabase.insert(TableData.TableInfo.TABLE_NAME, null, v);
    }

    public List<MyMarkerObject> getMarkers(){
        List<MyMarkerObject>markers = new ArrayList<MyMarkerObject>();
        Cursor cursor = sqLiteDatabase.query(TableData.TableInfo.TABLE_NAME, cols, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            MyMarkerObject m = cursorToMarker(cursor);
            markers.add(m);
            cursor.moveToNext();
        }
        cursor.close();
        return markers;
    }

    private MyMarkerObject cursorToMarker(Cursor cursor){
        MyMarkerObject m = new MyMarkerObject();
        m.setLatitude(cursor.getString(0));
        m.setLongit(cursor.getString(1));
        return m;
    }
}
