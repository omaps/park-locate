package utils;

import com.google.android.gms.maps.model.LatLng;
/**
 * Created by David Gormally on 11/06/15.
 */
public class DataHolder  {
    private static DataHolder dataHolder;
    private LatLng parkingCoords;

    public static DataHolder getDataHolder() {
        if(dataHolder == null) {
            dataHolder = new DataHolder();
        }
        return dataHolder;
    }


    public LatLng getParkingCoords() {
        return parkingCoords;
    }

    public void setParkingCoords(LatLng parkingCoords) {
        this.parkingCoords = parkingCoords;
    }
}
