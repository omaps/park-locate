package utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.widget.Toast;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;
/**
 * Created by David Gormally on 11/06/15.
 */
public class AddressFromLatLng {

    public static String getFullAddress(Context context, LatLng latLng) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(context);
            if (latLng.latitude != 0 || latLng.longitude != 0) {
                addresses = geocoder.getFromLocation(latLng.latitude ,
                        latLng.longitude, 1);
                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getAddressLine(1);
                String county = addresses.get(0).getAddressLine(2);
                Log.d("TAG", "address = " + address + ", city = " + city + ", country = " + county);
                return address + ", " + city + "  " + county;

            } else {
                Toast.makeText(context, "Couldn't find address, please try again",
                        Toast.LENGTH_LONG).show();
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getAddressLineOne(Context context, LatLng latLng) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(context);
            if (latLng.latitude != 0 || latLng.longitude != 0) {
                addresses = geocoder.getFromLocation(latLng.latitude ,
                        latLng.longitude, 1);
                String address = addresses.get(0).getAddressLine(0);

                Log.d("TAG", "address = " + address);
                return address;

            } else {
                Toast.makeText(context, "Couldn't find address, please try again",
                        Toast.LENGTH_LONG).show();
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static String getAddressLineTwo(Context context, LatLng latLng) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(context);
            if (latLng.latitude != 0 || latLng.longitude != 0) {
                addresses = geocoder.getFromLocation(latLng.latitude ,
                        latLng.longitude, 1);
                String city = addresses.get(0).getAddressLine(1);
                String county = addresses.get(0).getAddressLine(2);
                return city + ",  " + county;

            } else {
                Toast.makeText(context, "Couldn't find address, please try again",
                        Toast.LENGTH_LONG).show();
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }




}
